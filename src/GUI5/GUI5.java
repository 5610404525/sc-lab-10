package GUI5;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JColorChooser;
import javax.swing.JFrame;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JPanel;

public class GUI5 extends JFrame{
	private JPanel panel,panel2;
	private JFrame frame;
	private JColorChooser colour;
	private JMenuBar bar;
	private JMenu menu;
	private JMenuItem i1,i2,i3;
	
	public GUI5(){
		Build();
	}
	
	public void Build(){
		frame = new JFrame();
		panel = new JPanel();
		panel2 = new JPanel();
		bar = new JMenuBar();
		menu = new JMenu("Color");
		i1 = new JMenuItem("Red");
		i2 = new JMenuItem("Green");
		i3 = new JMenuItem("Blue");
		
		setLayout(new BorderLayout());
		add(panel,BorderLayout.NORTH);
		add(panel2,BorderLayout.CENTER);
		
		//panel1
		panel.setLayout(new BorderLayout());
		panel.add(bar,BorderLayout.WEST);
		bar.add(menu);
		menu.add(i1);
		menu.add(i2);
		menu.add(i3);
		
		//panel2
		panel2.setBackground(Color.white);
		
		i1.addActionListener(new ActionListener(){

			@Override
			public void actionPerformed(ActionEvent arg0) {
				// TODO Auto-generated method stub
				panel2.setBackground(Color.RED);
			}

	});
		
		i2.addActionListener(new ActionListener(){

			@Override
			public void actionPerformed(ActionEvent arg0) {
				// TODO Auto-generated method stub
				panel2.setBackground(Color.GREEN);
			}

	});
		
		i3.addActionListener(new ActionListener(){

			@Override
			public void actionPerformed(ActionEvent arg0) {
				// TODO Auto-generated method stub
				panel2.setBackground(Color.BLUE);
			}

	});

	}
	}
