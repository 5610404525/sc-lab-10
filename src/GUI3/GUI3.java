package GUI3;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JCheckBox;
import javax.swing.JColorChooser;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.Timer;


public class GUI3 extends JFrame{
	private JCheckBox green,red,blue;
	private JPanel panel,panel2;
	private JFrame frame;
	private JColorChooser colour;
	
	public GUI3(){
		Build();
	}
	
	public void Build(){
		frame = new JFrame();
		panel = new JPanel();
		panel2 = new JPanel();
		red =  new JCheckBox("Red");
		green = new JCheckBox("Green");
		blue = new JCheckBox("Blue");
		
		setLayout(new BorderLayout());
		add(panel,BorderLayout.CENTER);
		add(panel2,BorderLayout.SOUTH);
		
		//panel1
		panel.setBackground(Color.white);
		
		//panel2
		panel2.add(green,BorderLayout.WEST);
		panel2.add(red,BorderLayout.CENTER);
		panel2.add(blue,BorderLayout.EAST);
	
		red.addActionListener(new ActionListener(){

			@Override
			public void actionPerformed(ActionEvent arg0) {
				// TODO Auto-generated method stub
				//panel1
				panel.setBackground(Color.RED);
			}
	});
		
		green.addActionListener(new ActionListener(){

			@Override
			public void actionPerformed(ActionEvent arg0) {
				// TODO Auto-generated method stub
				//panel1
				panel.setBackground(Color.GREEN);
			}
	});
		
		blue.addActionListener(new ActionListener(){

			@Override
			public void actionPerformed(ActionEvent arg0) {
				// TODO Auto-generated method stub
				//panel1
				panel.setBackground(Color.BLUE);
			}
	});
		
		class TimerListener implements ActionListener{

			@Override
			public void actionPerformed(ActionEvent arg0) {
				// TODO Auto-generated method stub
				if(red.isSelected()&&green.isSelected()){
					panel.setBackground(Color.YELLOW);
				}
				if(red.isSelected()&&blue.isSelected()){
					panel.setBackground(Color.MAGENTA);
				}
				if(green.isSelected()&&blue.isSelected()){
					panel.setBackground(Color.CYAN);
				}
			}
	
		
		
	}
		ActionListener listener = new TimerListener();
		Timer t = new Timer(300,listener);
		t.start();
	
	
	
	}
}