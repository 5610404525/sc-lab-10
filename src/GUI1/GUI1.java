package GUI1;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JColorChooser;
import javax.swing.JFrame;
import javax.swing.JPanel;

public class GUI1 extends JFrame{
	
	private JButton b1,b2,b3;
	private JPanel panel,panel2;
	private JFrame frame;
	
	public void GUI1(){
		Build();
	}
	
	public void Build(){
		frame = new JFrame();
		panel = new JPanel();
		panel2 = new JPanel();
		b1 =  new JButton("Red");
		b2 = new JButton("Green");
		b3 = new JButton("Blue");
		
		setLayout(new BorderLayout());
		add(panel,BorderLayout.CENTER);
		add(panel2,BorderLayout.SOUTH);
		//panel1
		panel.setBackground(Color.white);
		
		//panel2
		panel2.setLayout(new BorderLayout());
		panel2.add(b2,BorderLayout.WEST);
		panel2.add(b1,BorderLayout.CENTER);
		panel2.add(b3,BorderLayout.EAST);
		
		b1.addActionListener(new ActionListener(){

			@Override
			public void actionPerformed(ActionEvent arg0) {
				// TODO Auto-generated method stub
				//panel1
				panel.setBackground(Color.red);
			}
	});
		
		b2.addActionListener(new ActionListener(){

			@Override
			public void actionPerformed(ActionEvent arg0) {
				// TODO Auto-generated method stub
				//panel1
				panel.setBackground(Color.green);
			}
	});
		
		b3.addActionListener(new ActionListener(){

			@Override
			public void actionPerformed(ActionEvent arg0) {
				// TODO Auto-generated method stub
				//panel1
				panel.setBackground(Color.blue);
			}
	});
		
	}
	

	}
	
